@component('mail::message')
    <h1 style="text-align: center">Order Invoice</h1>

<table style="margin:8px auto; text-align: center;width: 100%">
    <thead>
        <tr style="height: 52px">
            <th>Vendor</th>
            <th>Service Type</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>
    @foreach($order->items as $orderItem)
        <tr style="height: 28px;">
            <td>{{ $orderItem->vendor()->first()['name'] }}</td>
            <td>{{ $orderItem->serviceType()->first()['name'] }}</td>
            <td>{{ $orderItem->amount  }}</td>
        </tr>
    @endforeach
    <tr style="height: 28px;">
        <td></td>
        <td></td>
        <td style="font-weight: bold; font-size: larger">
            {{$order->total}}
        </td>
    </tr>
    </tbody>
</table>
@component('emails.orders.button', ['url_confirm' => route('process_invoice', ['encoded' => $encoded, 'action'=>'confirm']), 'url_cancel'=>route('process_invoice', ['encoded' => $encoded, 'action'=>'cancel'])])
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
