<table class="action" align="center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding: 12px">
                                    <a href="{{ $url_confirm }}" class="button button-{{ $color ?? 'primary' }}" target="_blank">Confirm Order</a>
                                </td>
                                <td style="padding: 12px">
                                    <a href="{{ $url_cancel }}" class="button button-{{ $color ?? 'error' }}" target="_blank">Cancel Order</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
