FROM php:7.0-cli

MAINTAINER Valentine Mayaki

RUN apt-get update
RUN apt-get install -y python-pip
RUN pip install -U pip
RUN pip install pywatch

WORKDIR /opt

ENTRYPOINT ["pywatch"]