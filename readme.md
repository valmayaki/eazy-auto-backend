## EazyAuto Backend API
This is a project for the backend api for EazyAuto loan platform written in PHP using the Laravel framework. This allows customers to book autoloan for their car repairs and payback in bits.

## How to run
- Clone the project into your computer
- Run application using docker by running `docker-compose up`
- Run Laravel artisan command using `./docker-artisan.sh <command>` e.g `./docker-artisan.sh migrate`
- Run PHPUnit test by running the command `./phpunit-run.sh`.