<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Models\Vendor::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'role' => $faker->name,
        'phone_number' => $faker->phoneNumber,
        'email' => $faker->email,
        'address_1' => $faker->address,
        'address_2' => '',
        'city' => $faker->city,
        'state' => $faker->city,
        'country' => 'Nigeria',
        'bank' => $faker->randomElement(['GTB', 'Zenith', 'First bank', 'access']),
        'bank_account_no' => $faker->bankAccountNumber,
        'type' => $faker->randomElement([\App\Models\Vendor::ADMIN_VENDOR, \App\Models\Vendor::CUSTOMER_VENDOR]),
        'added_by' => User::all()->random()->id,
    ];
});
