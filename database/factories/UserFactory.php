<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $email = $faker->unique()->safeEmail;
    $provider = $faker->randomElement([\App\User::DEFAULT_PROVIDER, 'google', 'facebook', 'linkedin']);
    if($provider == \App\User::DEFAULT_PROVIDER){
        $provider_id = $email;
        $password = bcrypt('password');
    }else{
        $provider_id = str_random();
        $password = null;
    }
    return [
        'name' => $faker->name,
        'email' => $email,
        'activated' => true,
        'email_verified_at' => now(),
        'password' => $password, // secret
        'remember_token' => str_random(10),
        'phone_number' => $faker->unique()->phoneNumber,
        'avatar_url' => $faker->imageUrl(),
        'address_1' => $faker->address,
        'address_2' => ' ',
        'city' => $faker->city,
        'state' => 'lagos',
        'country' => 'nigeria',
        'provider' => $provider,
        'provider_id' => $provider_id,
        'gender' => $faker->randomElement(['male', 'female']),
    ];
});
