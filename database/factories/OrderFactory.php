<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Order::class, function (Faker $faker) {
    $data = [
        'user_id' => \App\User::all()->random()->id,
        'transaction_ref' => str_random(10),
        'payment_method' => $faker->randomElement(['paystack', 'cash', 'wallet']),
        'status' => $faker->randomElement(['pending', 'processing', 'cancelled', 'complete']),
        'total' => $faker->numberBetween(1000, 20000)
    ];
    if($data['status'] == 'pending'){
        $data['is_paid'] = 0;
    }else if($data['status'] == 'cancelled'){
        $data['is_paid'] = $faker->randomElement([0, 1]);
    }else{
        $data['is_paid'] = 1;
    }
    $data['date_paid'] = $data['is_paid'] ? $faker->dateTimeBetween('-2 weeks') : null;
    return $data;
});
