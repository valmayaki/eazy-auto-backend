<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ServiceType::class, function (Faker $faker) {
    $name = $faker->realText(15);
    return [
        'name' => $name,
        'slug' => str_slug($name),
    ];
});
