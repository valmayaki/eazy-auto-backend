<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Vehicle::class, function (Faker $faker) {
    return [
        'plate_number' => $faker->unique()->text(10),
        'user_id' => \App\User::all()->random()->id,
        'make' => $faker->text(10)
    ];
});
