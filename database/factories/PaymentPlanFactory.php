<?php

use Faker\Generator as Faker;

$factory->define(App\Models\PaymentPlan::class, function (Faker $faker) {
    $months =$faker->randomElement([1,2,3,4,5]);
    return [
        "name" => sprintf("%d month loan", $months),
        "months"=> $months,
        "interest"=> 5,
        "interest_type"=> "interest on remaining"
    ];
});
