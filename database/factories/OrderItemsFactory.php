<?php

use Faker\Generator as Faker;

$factory->define(App\Models\OrderItem::class, function (Faker $faker) {
    return [
        'order_id' => \App\Models\Order::all()->random()->id,
        'vendor_id' => \App\Models\Vendor::all()->random()->id,
        'service_item_id' => \App\Models\ServiceType::all()->random()->id,
        'amount' => $faker->numberBetween(1000, 10000)
    ];
});
