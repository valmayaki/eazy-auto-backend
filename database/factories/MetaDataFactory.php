<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Metadata::class, function (Faker $faker) {
    return [
        'meta_key' => $faker->text(20),
        'meta_value' => $faker->text(30)
    ];
});
