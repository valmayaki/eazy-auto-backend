<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = \App\User::create([
            'name' => 'Auto Admin',
            'email' => 'victokala@gmail.com',
            'activated' => true,
            'email_verified_at' => now(),
            'password' => bcrypt('password'), // secret
            'remember_token' => str_random(10),
            'phone_number' => '07039873467',
            'avatar_url' => '',
            'address_1' => '',
            'address_2' => ' ',
            'city' => '',
            'state' => 'lagos',
            'country' => 'nigeria',
            'provider' => 'Eazy Auto',
            'provider_id' => 'victokala@gmail.com',
            'gender' => 'male',
        ]);
       $user->assign(\App\Role::ADMINISTRATOR);
       factory(\App\User::class)->create([
           'email' => 'victayo@ymail.com',
           'password' => bcrypt('password'),
           'provider' => \App\User::DEFAULT_PROVIDER
       ]);
        factory(\App\User::class, 50)->create();
    }
}
