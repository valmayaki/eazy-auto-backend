<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::allow(\App\Role::ADMINISTRATOR)->to(\App\Role::MANAGE_USERS);
        Bouncer::allow(\App\Role::ADMINISTRATOR)->to(\App\Role::MANAGE_ORDERS);
        Bouncer::allow(\App\Role::ADMINISTRATOR)->to(\App\Role::MANAGE_VENDORS);
    }
}
