<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 22/05/2019
 * Time: 2:19 PM
 */

return [
    'secret_key' => env('PAYSTACK_SECRET_KEY', ''),
    'public_key' => env('PAYSTACK_PUBLIC_KEY', ''),
    'url' => env('PAYSTACK_URL', 'https://api.paystack.co')
];