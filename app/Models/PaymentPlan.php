<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentPlan
 * @package App\Models
 *
 * @property string $name
 * @property int    $months
 * @property int    $interest
 * @property string $interest_type
 */
class PaymentPlan extends Model
{
    protected $fillable = [
        'name',
        'months',
        'interest',
        'interest_type',
    ];
}
