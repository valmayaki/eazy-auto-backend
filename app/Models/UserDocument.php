<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDocument extends Model
{
    const UTILITY_BILL = 'utility bill';
    const INTERNATIONAL_PASSPORT = 'international passport';
    const NATIONAL_ID = 'national ID';
    const DRIVER_LICENSE = 'driver licence';

    protected $fillable = [
        'user_id',
        'document',
        'type'
    ];
}
