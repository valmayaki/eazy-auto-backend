<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LoanSettlement
 * @package App\Models
 *
 * @property int    $loan_id
 * @property float  $amount
 * @property Carbon $due_at
 * @property Carbon $paid_at
 * @property string $reference
 * @property string $status
 */
class LoanSettlement extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'due_at',
        'paid_at'
    ];
    protected $fillable = [
        'loan_id',
        'amount',
        'due_at',
        'paid_at',
        'reference',
        'status',
    ];

    public function loan()
    {
        return $this->belongsTo(LoanRequest::class,'loan_id');
    }
}
