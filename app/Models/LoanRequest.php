<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LoanRequest
 * @package App\Models
 *
 * @property int $user_id
 * @property float amount
 * @property float $amount_to_be_paid
 * @property string $status
 * @property string $reference
 */
class LoanRequest extends Model
{
    protected $table = "loans";

    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount',
        'amount_to_be_paid',
        'status',
        'reference'
    ];

    /**
     * Defines the relationship between the loan request
     * and who requested for it
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function settlements()
    {
        return $this->hasMany(LoanSettlement::class, 'loan_id');
    }
}
