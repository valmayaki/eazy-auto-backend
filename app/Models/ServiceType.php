<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ServiceType
 * @package App\Models
 *
 * @property int    $id
 * @property string $name
 * @property string $slug
 */
class ServiceType extends Model
{
    protected $fillable = [
      'name',
      'slug'
    ];
}
