<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    const SUCCESS = 'success';
    const ABANDONED = 'abandoned';

    protected $fillable = [
        'user_id',
        'payment_method',
        'reference',
        'status',
        'amount',
        'data',
        'purpose'
    ];

    public function getData(){
        return json_decode($this->data, true);
    }
}
