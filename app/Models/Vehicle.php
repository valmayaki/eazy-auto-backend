<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Vehicle
 * @package App\Models
 *
 * @property int    $id
 * @property string $plate_number Unique string representing the plate-number
 */
class Vehicle extends Model
{
    protected $fillable = [
        'plate_number',
        'model',
        'year',
        'user_id'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
