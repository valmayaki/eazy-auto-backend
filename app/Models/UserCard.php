<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserCard
 * @package App\Models
 *
 * @property int    $id
 * @property string $email
 * @property string $authorization_code
 * @property int $user_id
 * @property boolean $is_default
 */
class UserCard extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'email',
        'authorization_code',
        'is_default'
    ];
}
