<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderItems
 * @package App\Models
 *
 * @property int    $order_id        The order the item belongs to
 * @property int    $vendor_id       Identifier for a vendor for this service
 * @property string $service_type    Service for this orderItem
 * @property int    $service_item_id Optional identifier for a product from a service
 * @property float  $amount          Amount for order item
 * @property string $status
 */
class OrderItem extends Model
{
    protected $fillable = [
        'order_id',
        'vendor_id',
        'service_type',
        'service_item_id',
        'status',
        'type',
        'amount'
    ];

    protected $with = ['vendor', 'serviceType'];
//    protected $touches = ['order'];
    /**
     * Get the order which the item belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(OrderItem::class, 'order_id');
    }

    public function serviceType(){
        return $this->hasOne(ServiceType::class, 'id', 'service_type');
    }

    public function vendor(){
        return $this->hasOne(Vendor::class, 'id', 'vendor_id');
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'order' => $this->order_id,
            'vendor' => $this->vendor()->get(),
            'serviceType' => $this->serviceType()->get(),
            'status' => $this->status,
            'type' => $this->type,
            'amount' => $this->amount
        ];
    }
}
