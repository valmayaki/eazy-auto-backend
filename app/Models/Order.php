<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Order
 * @package App\Models
 *
 * @property int    $id
 * @property int    $user_id
 * @property float  $total
 * @property string $payment_method
 * @property string $transaction_ref
 * @property string $status
 */
class Order extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'total',
        'payment_method',
        'transaction_ref',
        'status',
        'is_paid',
        'date_paid'
    ];

    protected $with = ['items'];

    /**
     * Defines the relation of the order to user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id');
    }

    public function paymentDetails(){
        return $this->hasMany(Transaction::class, 'reference', 'transaction_ref');
    }
}
