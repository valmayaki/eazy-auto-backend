<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Vendor
 * @package App\Models
 *
 * @property int    $id
 * @property string $name
 * @property string $role
 * @property string $phone_number
 * @property string $email
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $bank
 * @property string $bank_account_no
 * @property string $added_by
 */
class Vendor extends Model
{
    const DEFAULT_META_DATA_KEY = 'default_vendor';
    const ADMIN_VENDOR = 'admin';
    const CUSTOMER_VENDOR = 'customer';
    protected $fillable = [
        'name',
        'role',
        'phone_number',
        'email',
        'address_1',
        'address_2',
        'city',
        'state',
        'country',
        'bank',
        'bank_account_no',
        'added_by',
        'type'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'added_by');
    }

    public function toArray()
    {
        $vendor = parent::toArray();
        $vendor['default'] = $this->isDefault();
        return $vendor;
    }

    public function isDefault(){
        if(!$this->id){
            return false;
        }
        $defaultVendor = Metadata::where('meta_key', self::DEFAULT_META_DATA_KEY)->first();
        return $this->id == $defaultVendor;
    }
}
