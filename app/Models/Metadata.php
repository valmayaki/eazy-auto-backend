<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Metadata extends Model
{
    protected $table = 'meta_data';
    protected $fillable = ['meta_key', 'meta_value'];
}
