<?php

namespace App\Listeners;

use App\Events\OrderPaid;
use App\Mail\OrderPaidReceipt;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderPaidListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderPaid $event
     * @return void
     */
    public function handle(OrderPaid $event)
    {
        \Mail::to($event->order->owner)->send(new OrderPaidReceipt($event->order));
    }
}
