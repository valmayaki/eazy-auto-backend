<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use App\Mail\OrderCreatedInvoice;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderInvoice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderCreated $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        \Mail::to($event->user)->send(new OrderCreatedInvoice($event->order));
    }
}
