<?php

namespace App\Mail;

use App\Models\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCreatedInvoice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    public $order;

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    protected $encoded;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->user = $order->owner;
        $arr = [
            'email' => $this->user->email,
            'order_id' => $order->id
        ];
        $this->encoded = base64_encode(json_encode($arr));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.order-created')
            ->subject('Order Invoice')
            ->with('encoded', $this->encoded);
    }
}
