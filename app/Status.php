<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 29/05/2019
 * Time: 4:24 PM
 */

namespace App;


class Status
{
    const PENDING = 'pending';
    const PROCESSING = 'processing';
    const CANCELLED = 'cancelled';
}