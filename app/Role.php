<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 22/06/2019
 * Time: 11:27 AM
 */

namespace App;


class Role
{
    const ADMINISTRATOR = 'administrator';
    const MANAGE_USERS = 'manage_users';
    const MANAGE_ORDERS = 'manage_orders';
    const MANAGE_VENDORS = 'manage_vendors';
}