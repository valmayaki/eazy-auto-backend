<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 22/05/2019
 * Time: 2:26 PM
 */

namespace App\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;

class PaystackService
{
    /**
     * @var Client
     */
    private $client;
    private $authorization;
    private $baseUrl;

    public function __construct()
    {
        $this->client = new Client();
        $this->authorization = [
            'Authorization' => 'Bearer '.config('paystack.secret_key')
        ];
        $this->baseUrl = config('paystack.url');
    }

    /**
     * @param $bvn
     * @return array
     *
     * Resolves BVN
     */
    public function resolveBVN($bvn){
        $url = "$this->baseUrl/bank/resolve_bvn/{$bvn}";
        return $this->makeConnection($url);
    }

    /**
     * @param $reference
     * @return array
     * Verify transaction reference
     */
    public function verifyPayment($reference){
        $url = "$this->baseUrl/transaction/verify/$reference";
        return $this->makeConnection($url);
    }

    private function makeConnection($url){
        try{
            $response = $this->client->request('GET', $url, [
                'headers' => $this->authorization
            ]);
            $body = \GuzzleHttp\json_decode($response->getBody(), true);
            return [
                'status_code' => $response->getStatusCode(),
                'body' => $body
            ];
        }catch (ClientException $exception){
            $response = $exception->getResponse();
            $responseBody = \GuzzleHttp\json_decode($response->getBody(), true);
            $status = $response->getStatusCode();
            return [
                'status_code' => $status,
                'body' => $responseBody
            ];
        } /**
         * @todo Handle exception when connection can not be established
         */
//        catch (ConnectException $exception){
//            $response = $exception->getMessage();
//        }
    }
}