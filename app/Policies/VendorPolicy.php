<?php

namespace App\Policies;

use App\Models\Vendor;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class VendorPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine whether the user can view the order.
     *
     * @param  \App\User $user
     * @param Vendor $vendor
     * @return mixed
     */
    public function view(User $user, Vendor $vendor)
    {
        return \Bouncer::can(Role::MANAGE_VENDORS) || $vendor->added_by == $user->id;
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param  \App\User $user
     * @param Vendor $vendor
     * @return mixed
     */
    public function update(User $user, Vendor $vendor)
    {
        return \Bouncer::can(Role::MANAGE_VENDORS) || $vendor->added_by == $user->id;
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param  \App\User $user
     * @param Vendor $vendor
     * @return mixed
     */
    public function delete(User $user, Vendor $vendor)
    {
        return \Bouncer::can(Role::MANAGE_VENDORS) || $vendor->added_by == $user->id;
    }

}
