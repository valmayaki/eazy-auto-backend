<?php

namespace App;

use App\Models\Transaction;
use App\Models\UserCard;
use App\Models\UserDocument;
use App\Models\UserMeta;
use App\Models\Vehicle;
use App\Models\Vendor;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Notifications\ResetPassword;
use App\Models\Order;

/**
 * Class User
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $phone_number
 * @property string $avatar_url
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string state
 * @property string country
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasApiTokens, CanResetPassword, HasRolesAndAbilities, SoftDeletes;

    const DEFAULT_PROVIDER = 'Eazy Auto';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone_number',
        'activated',
        'gender',
        'provider',
        'provider_id',
        'avatar_url',
        'address_1',
        'address_2',
        'city',
        'state',
        'country',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Check if a user is an administrator
     *
     * @return bool
     */
    public function isAdmin()
    {
        return false;
    }

    public function meta()
    {
        return $this->hasMany(UserMeta::class, 'user_id');
    }

    public function getFirstNameAttribute()
    {
        $firstName = '';
        $lastName= '';
        sscanf($this->name, '%s  %s', $firstName, $lastName);
        return $firstName;
    }

    public function getLastNameAttribute()
    {
        $firstName = '';
        $lastName= '';
        sscanf($this->name, '%s  %s', $firstName, $lastName);
        return $lastName;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get Users Vehicles
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'user_id');
    }

    /**
     * Get Vendors added by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vendors()
    {
        return $this->hasMany(Vendor::class, 'added_by');
    }
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
    /**
     * Get orders for a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function getFullnameAttribute(){
        return ucwords("$this->name");
    }

    public function cards(){
        return $this->hasMany(UserCard::class);
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }

    public function documents(){
        return $this->hasMany(UserDocument::class);
    }
}
