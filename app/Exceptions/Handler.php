<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof UnauthorizedHttpException) {
            $preException = $exception->getPrevious();
            $result = $this->handleJwTException($exception,$preException);
            if(!is_null($result)){
                return $result;
            }

        }
        return parent::render($request, $exception);
    }

    private function handleJwTException($exception, $preException = null)
    {
        if ($preException instanceof
            \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            return response()->json(['error' => 'TOKEN_EXPIRED'], Response::HTTP_FORBIDDEN);
        } else if ($preException instanceof
            \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            return response()->json(['error' => 'TOKEN_INVALID'], Response::HTTP_FORBIDDEN);
        } else if ($preException instanceof
            \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {
            return response()->json(['error' => 'TOKEN_BLACKLISTED'], Response::HTTP_FORBIDDEN);
        }
        if ($exception->getMessage() === 'Token not provided') {
            return response()->json(['error' => 'Token not provided'], Response::HTTP_FORBIDDEN);
        }
        return null;
    }
}
