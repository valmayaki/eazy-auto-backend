<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{

    public function redirect($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function handleCallback($provider){
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        /**
         * @todo handle possible error???
         */
        $user =   User::where(['email' => $userSocial->getEmail()])->first();
        if(!$user){
            $user = User::create([
                'name'          => $userSocial->getName(),
                'email'         => $userSocial->getEmail(),
                'avatar_url'    => $userSocial->getAvatar(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
                'activated'     => true
            ]);
            /**
             * @todo forcefill verified at when creating new user
             */
//            $user->forceFill([
//                'email_verified_at' => now()
//            ]);
//            $user->save();
        }

        $token = \Auth::login($user);
        $responseData = [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => \Auth::guard('jwt')->factory()->getTTL() * 60
        ];
        return response()->json($responseData);
    }
}
