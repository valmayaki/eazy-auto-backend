<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function doUnauthorized(string $message = null){
        $message = isset($message) ? $message : 'This action is unauthorized.';
        return response()->json(['status' => false, 'message' => $message], Response::HTTP_UNAUTHORIZED);
    }

    protected function doErrorResponse(string $message = null, $serverError = false){
        $message = isset($message) ? $message : 'An error occurred while performing request';
        $status = $serverError ? Response::HTTP_INTERNAL_SERVER_ERROR : Response::HTTP_BAD_REQUEST;
        return response()->json(['status' => false, 'message' => $message], $status);
    }
}
