<?php

namespace App\Http\Controllers\Api;

use App\Models\UserDocument;
use App\Repositories\UserDocumentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class UserDocumentController extends Controller
{
    /**
     * @var UserDocumentRepository
     */
    private $userDocumentRepository;

    /**
     * UserDocumentController constructor.
     * @param UserDocumentRepository $userDocumentRepository
     */
    public function __construct(UserDocumentRepository $userDocumentRepository)
    {
        $this->userDocumentRepository = $userDocumentRepository;
    }


    public function index(Request $request){
        $params = $request->query();
        $documents = $this->userDocumentRepository->getAllDocuments($params);
        return response()->json($documents);
    }

    public function create(Request $request){
        $rules = [
            'user' => 'required|exists:users,id',
            'document' => 'required|file|max:5000',
            'type' => 'required|string'
        ];
        $data = $request->all();
        $validator = \Validator::make($data, $rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'message' => $validator->messages()
            ], Response::HTTP_BAD_REQUEST);
        }
        $path = $request->document->store($this->userDocumentRepository->getStoragePath());
        $data['document'] = $path;
        $response = $this->userDocumentRepository->createDocument($data);
        return response()->json($response);
    }

    public function show(UserDocument $document){
        return response()->json($document);
    }

    public function destroy(UserDocument $document){
        try{
            $success = $this->userDocumentRepository->removeDocument($document);
            return response()->json($success);
        }catch (\Exception $exception){
            return response()->json(false, Response::HTTP_BAD_REQUEST);
        }
    }
}
