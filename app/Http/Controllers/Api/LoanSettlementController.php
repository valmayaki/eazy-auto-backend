<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LoanSettlement;
use Illuminate\Http\Request;

class LoanSettlementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoanSettlement  $loanSettlement
     * @return \Illuminate\Http\Response
     */
    public function show(LoanSettlement $loanSettlement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoanSettlement  $loanSettlement
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanSettlement $loanSettlement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoanSettlement  $loanSettlement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoanSettlement $loanSettlement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoanSettlement  $loanSettlement
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanSettlement $loanSettlement)
    {
        //
    }
}
