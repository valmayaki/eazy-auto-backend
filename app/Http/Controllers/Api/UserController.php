<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Role;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        if (\Bouncer::can(Role::MANAGE_USERS)) {
            $params = $request->query();
            $users = $this->userRepository->search($params);
            return response()->json($users);
        } else {
            return $this->doUnauthorized();
        }
    }

    public function show(User $user)
    {
        try {
            $this->authorize($user);
            return response()->json($user);
        } catch (AuthorizationException $authorizationException) {
            return $this->doUnauthorized($authorizationException->getMessage());
        } catch (\Exception $exception) {
            return $this->doErrorResponse($exception->getMessage());
        }
    }

    public function update(Request $request, User $user)
    {
        try {
            $this->authorize($user);
            $data = $request->except(['id', 'email', 'activated', 'password', 'provider', 'provider_id']);
            if (empty($data)) {
                return response()->json(
                    [
                        'errors' => ['message' => 'No data provided for update'],
                        'user' => $user
                    ],
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );
            }
            $user->update($data);
            return response()->json($user);
        } catch (AuthorizationException $authorizationException) {
            return $this->doUnauthorized($authorizationException->getMessage());
        } catch (Exception $ex) {
            logger($ex);
            return $this->doErrorResponse($ex->getMessage());
        }
    }

    public function destroy(User $user){
        try {
            $this->authorize($user);
            $stat = $this->userRepository->deleteUser($user);
            return response()->json($stat);
        }
        catch (AuthorizationException $authorizationException){
            return $this->doUnauthorized($authorizationException->getMessage());
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
