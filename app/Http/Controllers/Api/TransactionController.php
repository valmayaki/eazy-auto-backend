<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\TransactionCollection;
use App\Repositories\TransactionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function index(Request $request){
        $params = $request->query();
        $response = $this->transactionRepository->getTransactions($params);
        $transactions = new TransactionCollection($response);
        return response()->json($transactions);
    }

    public function delete(){

    }
}
