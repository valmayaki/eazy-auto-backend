<?php

namespace App\Http\Controllers\Api;

use App\Events\OrderCreated;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrdersCollection;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Role;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrdersCollection
     */
    private $ordersCollection;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->query();
        if (\Bouncer::can(Role::MANAGE_ORDERS)) {
            $orders = $this->orderRepository->getAllOrders($params);
        } else {
            $params['user'] = Auth::id();
            $orders = $this->orderRepository->getAllOrders($params);
        }
        $col = $this->getOrdersCollection($orders);
        return response()->json($col);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'payment_method' => [
                'required',
                'string'
            ],
            'order_items' => ['nullable', 'bail', 'array'],
            'order_items.*.vendor_id' => [
                'required',
                'integer',
                'exists:vendors,id'
            ],
            'order_items.*.vehicle_id' => [
                'required',
                'integer',
                'exists:vehicles,id'
            ],
            'order_items.*.amount' => [
                'sometimes',
                'required',
                'numeric'
            ],
            'order_items.*.service_type' => [
                'required',
                'int'
            ],
        ];
        if (\Bouncer::can(Role::MANAGE_ORDERS)) {
            $rules['user'] = [
                'required',
                'exists:users,id'
            ];
        }
        $orderData = $request->all();
        $validator = \Validator::make($orderData, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()
            ], Response::HTTP_BAD_REQUEST);
        }
        $orderItems = isset($orderData['order_items']) ? $orderData['order_items'] : [];
        $total = collect($orderItems)->reduce(function ($accum, $item) {
            $amount = isset($item['amount']) ? $item['amount'] : 0;
            return $accum + $amount;
        }, 0);
        $orderData['total'] = floatval($total);
        $orderData['transaction_ref'] = \str_random(9);
        if (!\Bouncer::can(Role::MANAGE_ORDERS)) {
            $orderData['user_id'] = \Auth::id();
        } else {
            $orderData['user_id'] = $orderData['user'];
        }
        try {
            $order = $this->orderRepository->createOrder($orderData);
            if ($order) {
//                $this->sendOrderCreatedEvent($order);
                return response()->json($order, Response::HTTP_CREATED);
            }
            return $this->doErrorResponse('unable to create order');
        } catch (\Exception $exception) {
            return $this->doErrorResponse($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Order $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        try {
            $this->authorize($order);
            return response()->json($this->orderRepository->getOrder($order->id));
        } catch (AuthorizationException $authorizationException) {
            return $this->doUnauthorized($authorizationException->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Order $order
     * @return Response
     */
    public function update(Request $request, Order $order)
    {
        $rules = [
            'payment_method' => [
                'sometimes',
                'string'
            ],
            'items' => ['nullable', 'bail', 'array'],
            'items.*.id' => [
                'required',
                'integer',
                'exists:order_items,id'
            ],
            'items.*.amount' => [
                'sometimes',
                'required',
                'numeric'
            ]
        ];
        $orderData = $request->all();
        $validator = \Validator::make($orderData, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()
            ], Response::HTTP_BAD_REQUEST);
        }
        try {
            $this->authorize($order);
            $order = $this->orderRepository->updateOrder($order, $orderData);
            return response()->json($order);
        } catch (AuthorizationException $authorizationException) {
            return $this->doUnauthorized($authorizationException->getMessage());
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        try {
            $this->authorize($order);
            $deleted = $this->orderRepository->deleteOrder($order);
            return response()->json($deleted);
        } catch (AuthorizationException $authorizationException) {
            return $this->doUnauthorized($authorizationException->getMessage());
        } catch (\Exception $exception) {
            return $this->doErrorResponse($exception->getMessage());
        }
    }

    /**
     * @return OrderRepository
     */
    public function getOrderRepository(): OrderRepository
    {
        return $this->orderRepository;
    }

    /**
     * @param OrderRepository $orderRepository
     * @return OrderController
     */
    public function setOrderRepository(OrderRepository $orderRepository): OrderController
    {
        $this->orderRepository = $orderRepository;
        return $this;
    }

    /**
     * @param $orders
     * @return OrdersCollection
     */
    public function getOrdersCollection($orders): OrdersCollection
    {
        return new OrdersCollection($orders);
    }

    /**
     * @param OrdersCollection $ordersCollection
     * @return OrderController
     */
    public function setOrdersCollection(OrdersCollection $ordersCollection): OrderController
    {
        $this->ordersCollection = $ordersCollection;
        return $this;
    }

    protected function sendOrderCreatedEvent($order)
    {
        event(new OrderCreated($order));
    }

}
