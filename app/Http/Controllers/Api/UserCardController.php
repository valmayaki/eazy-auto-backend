<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\CardRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserCardController extends Controller
{
    /**
     * @var CardRepository
     */
    private $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    public function index(){
        $user = \Auth::user();
        $cards = $this->cardRepository->getCards($user);
        return response()->json($cards);
    }

    public function create(Request $request)
    {
        $params = $request->all();
        $rules = [
            'email' => 'required|email',
            'authorization_code' => 'required|string',
        ];
        $validator = \Validator::make($params, $rules);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->messages()], Response::HTTP_BAD_REQUEST);
        }
        try {
            $user = \Auth::user();
            $response = $this->cardRepository->addCard($user, $params);
            $success = !is_null($response);
            return response()->json(['status' => $success, 'data' => $response]);
        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy($card){
        $user = \Auth::user();
        $success = $this->cardRepository->removeCard($user, $card);
        return response()->json(['status' => $success]);
    }

    public function setCardDefault($card){
        $user = \Auth::user();
        $response = $this->cardRepository->setDefaultCard($user, $card);
        return response()->json(['status' => !is_null($response), 'data' => $response]);
    }

}
