<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ServiceType;
use App\Repositories\ServiceTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ServiceTypeController extends Controller
{

    /**
     * @var ServiceTypeRepository
     */
    private $seviceTypeRepository;

    /**
     * ServiceTypeController constructor.
     * @param ServiceTypeRepository $seviceTypeRepository
     */
    public function __construct(ServiceTypeRepository $seviceTypeRepository)
    {
        $this->seviceTypeRepository = $seviceTypeRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $serviceTypes = $this->seviceTypeRepository->getAllServiceTypes($request->query());
        return response()->json($serviceTypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:service_types,name',
            'slug' => 'sometimes|required|string|max:255'
        ]);
        if($validate->fails()){
            return response()->json([
                'status' => false,
                'message' => $validate->messages()
            ], Response::HTTP_BAD_REQUEST);
        }
        $slug = $request->input('slug', null);
        if(!$slug){
            $slug = str_slug($request->input('name', ''), '-');
        }
        $serviceTypeArr = $request->all();
        $serviceTypeArr['slug'] = $slug;
        $serviceType = ServiceType::create($serviceTypeArr);
        return response()->json($serviceType, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param ServiceType $serviceType
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceType $serviceType)
    {
        return response()->json($serviceType);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param ServiceType $serviceType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceType $serviceType)
    {
        $serviceType->fill($request->all())->save();
        return response()->json($serviceType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ServiceType $serviceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceType $serviceType)
    {
        try{
            $deleted = $serviceType->delete();
            $success = !is_null($deleted);
            return response()->json($success);
        }catch(\Exception $exception){
            return response()->json(false, Response::HTTP_BAD_REQUEST);
        }
    }
}
