<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PaymentPlan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = PaymentPlan::all();
        return response()->json($plans);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plans = PaymentPlan::create($request->all());
        return response()->json($plans);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaymentPlan  $paymentPlan
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentPlan $paymentPlan)
    {
        return response()->json($paymentPlan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaymentPlan  $paymentPlan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentPlan $paymentPlan)
    {
        $paymentPlan->fill($request->all());
        $paymentPlan->save();
        return response()->json($paymentPlan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaymentPlan $paymentPlan
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentPlan $paymentPlan)
    {
        try{
            $deleted = $paymentPlan->delete();
            $del = !is_null($deleted);
            return response()->json($del);
        }catch (\Exception $exception){
            return response()->json(false, Response::HTTP_BAD_REQUEST);
        }
    }
}
