<?php

namespace App\Http\Controllers\Api;

use App\User;
use Auth;
use Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Password;
use App\Services\ActivationService;

class JWTAuthController extends Controller
{
    use SendsPasswordResetEmails, ResetsPasswords;
//    use RegistersUsers;

    /**
     * Activation Service
     *
     * @var \App\Services\ActivationService
     */
    protected $activationService;

    /**
     * Create a new AuthController instance.
     *
     * @param ActivationService $activationService
     */
    public function __construct(ActivationService $activationService)
    {
        $this->activationService =$activationService;
        $this->middleware('jwt.auth', ['except' => ['login', 'register','reset', 'sendResetLinkEmail']]);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @return array
     */
    protected function validator()
    {

        $rules = [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone_number' => ['required', 'string', 'max:14', 'min:11', 'unique:users'],
            'password' => ['required', 'string', 'min:6',],
        ];
        return $rules;
    }


    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {

        $rules = $this->validator($request->all());
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->messages()
            ], 499);
        }
        $user = $this->create($request->all());
        event(new Registered($user));

        $this->activationService->sendActivationMail($user);
        return response()->json(['success'=>true, 'message'=> 'A link has been sent to your email, please follow the link to activate your account'], Response::HTTP_CREATED);
    }

    /**
     * @inheritDoc
     * 
     */
    public function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json(['status' => true, "message" => trans($response)]);
    }

    /**
     * @inheritDoc
     * 
     */
    public function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['status' => false, "message" => trans($response)], 404);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse(Request $request, $response)
    {
        $data = ['status' => true, "message" => trans($response)];
        return response()->json($data);
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response()->json(['status' => false, "message" => trans($response)], 404);
    }
    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('jwt');
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => sprintf('%s  %s', trim($data['first_name']),trim($data['last_name'])) ,
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'password' => bcrypt($data['password']),
            'provider' => config('app.name'),
            'provider_id' => $data['email']
        ]);
    }

    /**
     * API Login, on success return JWT Auth token
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];
        $this->validate($request, $rules);
        try {
            // Attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => 'error',
                    'error_code' => 'auth_invalid_credentials',
                    'message' => 'We can`t find an account with this credentials.'
                ], 401);
            }
        } catch (JWTException $e) {
            // Something went wrong with JWT Auth.
            return response()->json([
                'status' => 'error',
                'error_code' => 'auth_failure',
                'message' => 'Failed to login, please try again.'
            ], 500);
        }
        $user =$this->guard()->getUser();
        if(!$user->activated){
            return response()->json([
                'status' => 'error',
                'error_code' => 'auth_not_activated',
                'message' => sprintf('Your account has not been activated, Please click on the link in your email or <a href="%s">click here</a> to resend activation code',route('auth.resend_activation', ['email' => $user->email])),
            ], 403);
        }
        // All good so return the token
        return $this->respondWithToken($token);
    }

    /**
     * Logout
     * Invalidate the token. User have to relogin to get a new token.
     * @param Request $request 'header'
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        // Get JWT Token from the request header key "Authorization"
//        $tokenHeader = $request->header('Authorization');
        // Invalidate the token
        try {
            $this->guard()->logout();
            return response()->json([
                'status' => 'success',
                'message'=> "User successfully logged out."
            ]);
        } catch (JWTException $e) {
            \Log::error($e);
            // something went wrong whilst attempting to encode the token
            return response()->json([
                'status' => 'error',
                'error_code' => 'auth_logout_failed',
                'message' => 'Failed to logout, please try again.'
            ], 500);
        }
    }

    /**
     * Return response with token
     *
     * @param Authenticatable $user
     * @param Request|null $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithJWtToken(Authenticatable $user, Request $request = null, $data= [])
    {
        $token = $this->guard()->tokenById($user->getAuthIdentifier());
        $responseData = [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ];
        if(!empty($data)){
            $responseData = array_merge($responseData, $data);
        }
        return response()->json($responseData);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     * @param array $data
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $data = [])
    {
        $responseData = [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60,
            'user' => Auth::user()
        ];
        if(!empty($data)){
            $responseData = array_merge($responseData, $data);
        }
        return response()->json($responseData);
    }

    protected function registered(Request $request, $user)
    {
        return $this->respondWithJWtToken($user, $request);
    }

     /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }

    public function resendActivation(Request $request)
    {
        $rules =['email'=>'required|email'];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'error_code' => 'email_invalid',
                'message' => 'Please provide a valid email',
            ], 422);
        }
        if(!$request->email){
            
        }
        $user = $this->guard()->getProvider()->retrieveByCredentials(['email' => $request->email]);
        if (!$user){
            return response()->json([
                'status' => 'error',
                'error_code' => 'user_invalid',
                'message' => sprintf('Your account has not been activated, Please click on the link in your email or <a href="%s">click here</a> to resend activation code', '#'),
            ], 404);
        }
        $this->activationService->sendActivationMail($user);
        return response()->json([
            'status' => 'success',
            'message' => "Activation mail resent",
        ], 200);
    }
}
