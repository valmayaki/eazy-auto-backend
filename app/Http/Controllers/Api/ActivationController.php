<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ActivationService;
class ActivationController extends Controller
{
    /**
     * Create a new ActivationController instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        $this->activationService =$activationService;
    }

    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            return redirect(sprintf("%s?message=%s", config('app.client_url'), "Your account has been successfully activated"));
        }
        abort(404);
    }
}
