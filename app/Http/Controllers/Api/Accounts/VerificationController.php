<?php

namespace App\Http\Controllers\Api\Accounts;

use App\Repositories\OrderRepository;
use App\Repositories\TransactionRepository;
use App\Services\PaystackService;
use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class VerificationController extends Controller
{
    /**
     * @var PaystackService
     */
    private $paystackService;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var TransactionRepository
     */
    private $transactionRepository;

    public function __construct(PaystackService $paystackService, OrderRepository $orderRepository, TransactionRepository $transactionRepository)
    {
        $this->paystackService = $paystackService;
        $this->orderRepository = $orderRepository;
        $this->transactionRepository = $transactionRepository;
    }

    public function resolveBVN($bvn)
    {
        $response = $this->paystackService->resolveBVN($bvn);
        $status = $response['status_code'];
        $body = $response['body'];
        return response()->json($body, $status);
    }

    public function verifyPayment($reference, Request $request)
    {
        $po = $request->query('pay_order', null);
        $response = $this->paystackService->verifyPayment($reference);
        $status = $response['status_code']; // the status code of the http request to paystack. 200 for success, any other for failure
        $body = $response['body'];
        if ($status === Response::HTTP_OK) {
            $data = $body['data'];
            if ($body['status']) {
                $transactionData = [
                    'user_id' => \Auth::id(),
                    'status' => $data['status'],
                    'payment_method' => 'paystack',
                    'reference' => $data['reference'],
                    'amount' => $data['amount'],
                    'data' => json_encode($data)
                ];
                // save transaction in table
                $this->transactionRepository->saveTransaction($transactionData);
                if ($data['status'] === 'success' && $po) { // is user attempting to pay for order or is this just a payment verification?
                    $order = $this->transactionRepository->getOrder($reference);
                    if (!$order) { // no order with reference found
                        return response()->json(['status' => false, 'message' => 'Could not update order. Confirm reference and try again'], Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    if($order->status != Status::PENDING){ // only pending order can be paid for so as to avoid double payment
                        return response()->json(['status' => false, 'message' => 'only pending orders can be paid for'], Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    $response = $this->payForOrder($order);
                    if(!$response){ // an error occured for whatever reason
                        return response()->json(['status' => false, 'message' => 'Could not update order. Confirm reference and try again'], Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    /**
                     * @todo send receipt to customer's mail
                     */
//                    event(new OrderPaid($order));
                    return response()->json(['status' => true, 'message' => 'order successfully updated']);
                }
                return response()->json(['status' => $data['status'], 'data' => $data]);
            } else {
                // for whatever reason, the status returned by paystack is false
                /**
                 * @todo handle if status returned by paystack is false. What data is returned by paystack? Can it be saved in the transactions table?
                 *
                 */
                return response()->json(['status' => false, 'message' => $body['message']]);
            }
        } else {
            // an http error response from paystack.
            /**
             * @todo log response
             */
            return response()->json(['status' => false, 'data' => $body], $status);
        }
    }

    private function payForOrder($order){
        $orderUpdate = [
            'status' => Status::PROCESSING,
            'payment_method' => 'paystack',
            'is_paid' => true,
            'date_paid' => now()->format('Y-m-d G:i:s')
        ];
        try{
            return $this->orderRepository->updateOrder($order, $orderUpdate);
        }catch (\Exception $exception){
            // @todo log exception
            return null;
        }
    }

//    public function handleTransaction(Request $request){
//        $params = $request->all();
//        if(!isset($params['reference'])){
//            abort(404);
//        }
//        $reference = $params['reference'];
//        // verify the reference
//        $response = $this->verifyPayment($reference);
//        $data = $response->getData(true);
//        dd($data);
//    }
}
