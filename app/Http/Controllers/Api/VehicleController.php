<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = 10;
        /** @var \App\User $user */
        $user = $request->user();
        $vehiclesQuery = $user->vehicles();
        if($request->has('search') && !empty($request->get('search'))){
          $vehiclesQuery->where('plate_number', 'like', sprintf('%%%s%%', $request->get('search')));
        }
        if($request->has('plate_number') && !empty($request->get('plate_number'))){
          $vehiclesQuery->where('plate_number', $request['plate_number']);
        }
        if($request->get('per_page') && !empty($request['per_page'])){
            $perPage = $request['per_page'];
        }
        $vehicles = $vehiclesQuery->paginate($perPage);
        return response()->json($vehicles);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plateNumber = $request['plate_number'];
        $vehicle = Vehicle::where('plate_number', $plateNumber)->first();
        if($vehicle){
            return response()->json(array('message' =>'plate number already exist'), 409);
        }
        $vehicle = new Vehicle();
        $vehicle->fill($request->all());
        $request->user()->vehicles()->save($vehicle);
        return response()->json($vehicle, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        $data = $vehicle;
        return response()->json($data); 
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        $vehicle->fill($request->all());
        $vehicle->save();
        $data = $vehicle;
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle $vehicle
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();
        $data = array();
        return response()->json($data, 204);
    }
}
