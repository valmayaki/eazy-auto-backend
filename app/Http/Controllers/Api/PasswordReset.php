<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Password;
class PasswordReset extends Controller
{
    use SendsPasswordResetEmails, ResetsPasswords;
    /**
     * @inheritDoc
     * 
     */
    public function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json(['status' => true, "message" => trans($response)]);
    }

    /**
     * @inheritDoc
     * 
     */
    public function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['status' => false, "message" => trans($response)], 404);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse(Request $request, $response)
    {
        $data = ['status' => true, "message" => trans($response)];
        return $this->respondWithJWtToken($request->user('jwt'),$request, $data);
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response()->json(['status' => false, "message" => trans($response)], 404);
    }
    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('jwt');
    }

     /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }
}
