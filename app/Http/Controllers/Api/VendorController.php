<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use App\Repositories\VendorRepository;
use App\Role;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class VendorController extends Controller
{
    /**
     * @var VendorRepository
     */
    private $vendorRepository;

    /**
     * VendorController constructor.
     * @param VendorRepository $vendorRepository
     */
    public function __construct(VendorRepository $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->query();
        if (\Bouncer::can(Role::MANAGE_VENDORS)) {
            $vendor = $this->vendorRepository->getAllVendors($params);
        } else {
            $params['user'] = \Auth::id();
            $vendor = $this->vendorRepository->getAllVendors($params);
        }
        return response()->json($vendor);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'role' => 'required|string',
            'email' => 'required|email',
            'phone_number' => 'required',
            'address_1' => 'required',
            'address_2' => 'required',
            'bank' => 'required',
            'bank_account_no' => 'required|min:10',
            'city' => 'required|string',
            'country' => 'required|string',
            'state' => 'required|string',
            'type' => 'in:' . Vendor::ADMIN_VENDOR . ',' . Vendor::CUSTOMER_VENDOR
        ];
        $vendorData = $request->all();
        $validator = Validator::make($vendorData, $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => $validator->messages()
            ], Response::HTTP_BAD_REQUEST);
        }
        $default = false;
        $canManage = \Bouncer::can(Role::MANAGE_VENDORS);
        if (array_key_exists('type', $vendorData)) {
            $vendorType = $vendorData['type'];
            if ($vendorType == Vendor::ADMIN_VENDOR && !$canManage) {
                return $this->doUnauthorized('You are not authorized to create an admin vendor');
            } else {
                if (array_key_exists('default', $vendorData)) {
                    $default = boolval($vendorData['default']);
                }
            }
        }
        if (!array_key_exists('type', $vendorData)) {
            $vendorData['type'] = Vendor::CUSTOMER_VENDOR;
        }
        if (array_key_exists('added_by', $vendorData)) {
            if (!$canManage) { // only admin can create vendor for a user
                $vendorData['added_by'] = auth()->id();
            }
        } else {
            $vendorData['added_by'] = auth()->id();
        }
        $vendorData['default'] = $default;
        $vendor = $this->vendorRepository->createVendor($vendorData);
        if (null == $vendor) {
            return $this->doErrorResponse('An error occurred while creating vendor', true);
        }
        return response()->json($vendor, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        try {
            $this->authorize($vendor);
            return response()->json($vendor);
        } catch (AuthorizationException $authorizationException) {
            return $this->doUnauthorized($authorizationException->getMessage());
        }

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDefaultVendor()
    {
        $canManage = \Bouncer::can(Role::MANAGE_VENDORS);
        if(!$canManage){
            return $this->doUnauthorized();
        }
        $vendor = $this->vendorRepository->getDefaultVendor();
        return response()->json($vendor);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Vendor $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendor $vendor)
    {
        try{
            $this->authorize($vendor);
            $vendor = $this->vendorRepository->updateVendor($vendor, $request->all());
            if (null == $vendor) {
                return $this->doErrorResponse('An error occurred while updating vendor', true);
            }
            return response()->json($vendor);
        }catch (AuthorizationException $authorizationException){
            return $this->doUnauthorized($authorizationException->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        try {
            $this->authorize($vendor);
            $deleted = $vendor->delete();
            $data = !is_null($deleted);
            return response()->json($data);
        } catch (AuthorizationException $authorizationException) {
            return $this->doUnauthorized($authorizationException->getMessage());
        } catch (\Exception $exception) {
            return response()->json(false, Response::HTTP_BAD_REQUEST);
        }
    }
}
