<?php

namespace App\Http\Controllers;

use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Status;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * InvoiceController constructor.
     * @param UserRepository $userRepository
     * @param OrderRepository $orderRepository
     */
    public function __construct(UserRepository $userRepository, OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }

    public function __invoke($encoded, Request $request){
        $decoded = base64_decode($encoded, true);
        if(!$decoded){
            abort(404);
        }
        $action = $request->input('action');
        $data = json_decode($decoded, true);
        if(!isset($data['email']) || !isset($data['order_id'])){
            abort(404);
        }
        $userEmail = $data['email'];
        $user = $this->userRepository->findByEmail($userEmail);
        if(is_null($user)){
            abort(404);
        }
        $orderId = $data['order_id'];
        $order = $this->orderRepository->getOrder($orderId);
        if(is_null($order)){
            abort(404);
        }
        $status = $order->status;
        if($status !== Status::PENDING){
            /**
             * @todo Only pending orders can be processed. Redirect to an expired link or something
             */
            abort(404);
        }
        if($action == 'cancel'){
            /**
             * @todo redirect to a confirm action page.
             */
        }
        return redirect('/');
    }
}
