<?php

namespace App\Http\Resources;

class OrdersCollection extends Resources
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resources = parent::toArray($request);
        $resources['data'] = $this->collection->transform(function ($data) {
            $data['items'] = OrderItemResource::collection($data['items']);
            return $data;
        });
        return $resources;
    }
}
