<?php
/**
 * Created by Victor.
 * User: Victor
 * Date: 23/08/2019
 * Time: 3:48 AM
 */

namespace App\Http\Resources;


use App\Models\Vendor;
use App\Repositories\MetaDataRepository;
use App\Repositories\VendorRepository;

class VendorCollection extends Resources
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resources = parent::toArray($request);
        $defaultVendor = $this->getDefaultVendor();
        $resources['data'] = $this->collection->transform(function ($data) use ($defaultVendor) {
            $data['default'] = $defaultVendor == $data['id'];
            return $data;
        });
        return $resources;
    }

    public function getDefaultVendor(){
//        $metaDataRepository = resolve(MetaDataRepository::class);
        /**
         * @var VendorRepository $vendorRepository
         */
        $vendorRepository = resolve(VendorRepository::class);
        $default = $vendorRepository->getDefaultVendor();
        if($default){
            return $default->id;
        }
        return null;
//        return $metaDataRepository->getMetaValue(Vendor::DEFAULT_META_DATA_KEY);
    }
}