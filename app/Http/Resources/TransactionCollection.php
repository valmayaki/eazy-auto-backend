<?php

namespace App\Http\Resources;

class TransactionCollection extends Resources
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $resources = parent::toArray($request);
        $resources['data'] = $this->collection->transform(function ($data) {
            $data['data'] = json_decode($data['data'], true);
            return $data;
        });
        return $resources;
    }
}
