<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'service_type' => $this->serviceType,
            'vendor' => $this->vendor,
            'status' => $this->status,
            'type' => $this->type,
            'amount' => $this->amount,
            'created_at' => $this->created_at->format('d M Y - H:i:s'),
            'update_at' => $this->updated_at
        ];
    }
}
