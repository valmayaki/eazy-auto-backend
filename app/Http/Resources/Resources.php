<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 16/06/2019
 * Time: 2:00 PM
 */

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\AbstractPaginator;

class Resources extends ResourceCollection
{
    public function toArray($request)
    {
        $lastPage = $this->lastPage();
        return [
            'current_page' => $this->currentPage(),
            'last_page' => $lastPage,
            'per_page' => $this->perPage(),
            'total' => $this->total(),
            'prev_page_url' => $this->previousPageUrl(),
            'next_page_url' => $this->nextPageUrl(),
            'path' => AbstractPaginator::resolveCurrentPath(),
            'first_page_url' => $this->url(1),
            'last_page_url' => $this->url($lastPage),
            'from' => 1,
            'to' => $this->collection->count(),
        ];
    }
}