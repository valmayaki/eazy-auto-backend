<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\Vendor;
use App\Policies\OrderPolicy;
use App\Policies\UserPolicy;
use App\Policies\VendorPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Order::class => OrderPolicy::class,
        User::class => UserPolicy::class,
        Vendor::class => VendorPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

//        Passport::routes();
    }
}
