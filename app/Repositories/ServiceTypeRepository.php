<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 16/06/2019
 * Time: 1:29 PM
 */

namespace App\Repositories;


use App\Models\ServiceType;

class ServiceTypeRepository extends Repository {

    public function getAllServiceTypes($params){
        $where = $this->buildParams($params);
        $perPage = isset($params['per_page']) ? $params['per_page'] : $this->perPage;
        $page = isset($params['page']) ? $params['page'] : null;
        $vendor = ServiceType::where($where);
        if(isset($params['search'])){
            $vendor = $this->getSearchParams($vendor, $params['search']);
        }
        return $vendor->paginate($perPage, ['*'], 'page', $page);
    }

    private function getSearchParams($model, $search){
        $model->orWhere('slug', 'like', "%$search%")
            ->orWhere('name', 'like', "%$search%");
        return $model;
    }
}