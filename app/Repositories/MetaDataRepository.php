<?php
/**
 * Created by Victor.
 * User: Victor
 * Date: 23/08/2019
 * Time: 3:51 AM
 */

namespace App\Repositories;


use App\Models\Metadata;

class MetaDataRepository
{
    public function updateMetaData($metaKey, $metaValue){
        if(is_array($metaValue)){
            $metaValue = json_encode($metaValue);
        }
        $metaData = Metadata::where('meta_key', $metaKey)->first();
        if(is_null($metaData)){
            $metaData = Metadata::create([
                'meta_key' => $metaKey,
                'meta_value' => $metaValue
            ]);
        }else{
            $metaData->meta_value = $metaValue;
            $metaData->save();
        }
        return $metaData;
    }

    public function getMetaValue($metaKey){
        $metaData = Metadata::where('meta_key', $metaKey)->first();
        if(is_null($metaData)){
            return null;
        }
        $metaValue = $metaData->meta_value;
        $str = json_decode($metaValue, true);
        if(json_last_error() == JSON_ERROR_NONE){ // a valid json string
            return $str;
        }
        return $metaValue;
    }

    public function deleteMetaData($metaKey){
        $metaData = Metadata::where('meta_key', $metaKey)->first();
        if($metaData){
            return $metaData->delete();
        }
        return true;
    }
}