<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 22/05/2019
 * Time: 6:44 PM
 */

namespace App\Repositories;


use App\Models\Order;
use App\Models\OrderItem;
use App\User;

class OrderRepository
{

    /**
     * @param $id
     * @return Order
     *
     */
    public function getOrder($id)
    {
        return Order::find($id);
    }

    /**
     * @param array $orderData
     * @return Order|null
     * @throws \Exception
     */
    public function createOrder(array $orderData)
    {
        if (!isset($orderData['user_id'])) {
            return null;
        }
        try{
            $userId = $orderData['user_id'];
            $user = User::find($userId);
            if (is_null($user)) {
                return null;
            }
            $orderItems = isset($orderData['order_items']) ? $orderData['order_items'] : [];
            \DB::beginTransaction();
            $order = $user->orders()->create($orderData);
            $order->items()->createMany($orderItems);
            \DB::commit();
            return $order->load('items');
        }catch (\Exception $exception){
            \DB::rollBack();
            throw $exception;
        }

    }

    /**
     * @param Order $order
     * @param $orderData
     * @return Order
     * @throws \Exception
     */
    public function updateOrder(Order $order, $orderData)
    {
        $orderItems = isset($orderData['items']) ? $orderData['items'] : [];
        $ids = $order->items()->pluck('id');
        try {
            \DB::beginTransaction();
            foreach ($orderItems as $orderItem) {
                $id = $orderItem['id'];
                if(!$ids->contains($id)){
                    continue;
                }
                $item = OrderItem::find($id);
                $item->forceFill($orderItem);
                $item->save();
            }
            $order->fill($orderData)->save();
            if(count($orderItems)){
                $order->total = $this->calculateTotal($order->items()->get());
                $order->save();
            }
            \DB::commit();
            return $order;
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }

    }

    private function calculateTotal($orderItems)
    {
        $amount = 0;
        foreach ($orderItems as $orderItem) {
            $amount += floatval($orderItem->amount);
        }
        return $amount;
    }

    /**
     * @param Order $order
     * @return bool if any order has been deleted
     * @throws \Exception
     */
    public function deleteOrder(Order $order)
    {
        try {
            \DB::beginTransaction();
            $items = $order->items()->pluck('id')->toArray();
            OrderItem::destroy($items);
            $del = $order->delete();
            \DB::commit();
            return !is_null($del);
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }
    }

    public function getAllOrders(array $params = [])
    {
        $where = $this->buildParams($params);
        $perPage = isset($params['per_page']) ? $params['per_page'] : 10;
        $page = isset($params['page']) ? $params['page'] : null;
        return Order::where($where)->paginate($perPage, ['*'], 'page', $page);
    }

    private function buildParams($params) {
        $where = [];
        if(isset($params['status'])){
            $where[] = ['status', '=', $params['status']];
        }
        if(isset($params['before'])) {
            $where[] = ['created_at', '<=', $params['before']];
        }
        if(isset($params['after'])) {
            $where[] = ['created_at', '>=', $params['after']];
        }
        if(isset($params['user'])) {
            $where[] = ['user_id', '=', $params['user']];
        }
        return $where;
    }

}