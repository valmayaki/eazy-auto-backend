<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 06/06/2019
 * Time: 9:30 AM
 */

namespace App\Repositories;


use App\Models\UserCard;
use App\User;

class CardRepository extends Repository
{

    /**
     * @param User $user
     * @param array $cardDetails
     * @return UserCard
     * @throws \Exception
     */
    public function addCard(User $user, array $cardDetails){
        $cards = $this->getCards($user);
        $cardDetails['is_default'] = $cards->isEmpty(); // if it's the first card, set it to default
        $cardDetails['user_id'] = $user->id;
        $userCard = new UserCard($cardDetails);
        try{
            $success = $userCard->save();
            if($success){
                return $userCard;
            }
            return null;
        }catch (\Exception $exception){
            throw $exception;
        }
    }

    public function removeCard(User $user, $cardId){
        $card = UserCard::findOrFail($cardId);
        if($card->user_id != $user->id){
            return false;
        }
        try{
            return $card->delete();
        }catch(\Exception $exception){
            return false;
        }
    }

    public function setDefaultCard(User $user, $cardId){
        $card = UserCard::findOrFail($cardId);
        if($card->user_id != $user->id){
            return null;
        }
        $defaultCard = $this->getDefaultCard($user);
        if($defaultCard->id == $card->id){
            return $card;
        }
        $defaultCard->is_default = false;
        $defaultCard->save();
        $card->is_default = true;
        $card->save();
        return $card;
    }

    public function getCards(User $user){
        return $user->cards()->get();
    }

    public function getDefaultCard(User $user){
        return $user->cards()->where('is_default', '=', true)->first();
    }
}