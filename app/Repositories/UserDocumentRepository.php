<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 15/06/2019
 * Time: 8:06 PM
 */

namespace App\Repositories;


use App\Models\UserDocument;

class UserDocumentRepository extends Repository
{
    const DOCUMENT_PATH = 'documents';

    public function getAllDocuments($params){
        $where = $this->buildParams($params);
        $perPage = isset($params['per_page']) ? $params['per_page'] : $this->perPage;
        $page = isset($params['page']) ? $params['page'] : null;
        return UserDocument::where($where)->paginate($perPage, ['*'], 'page', $page);
    }

    public function createDocument($data){
        if(isset($data['user'])){
            $data['user_id'] = $data['user'];
        }
        $userDoc = UserDocument::create($data);
        if($userDoc){
            return $userDoc;
        }
        return null;
    }

    public function removeDocument(UserDocument $document){
        try{
            $file = $document->document;
            $success = $document->delete();
            if($success){
                \Storage::delete($file);
            }
            return $success;
        }catch (\Exception $exception){
            throw $exception;
        }
    }

    public function getStoragePath(){
        return self::DOCUMENT_PATH;
    }

    protected function buildParams($params) {
        $where = parent::buildParams($params);
        if(isset($params['search'])){
            $where[] = ['type', 'like', "%{$params['search']}%"];
        }
        if(isset($params['type'])){
            $where[] = ['type', 'like', $params['type']];
        }
        return $where;
    }
}