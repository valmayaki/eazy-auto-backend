<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 05/06/2019
 * Time: 6:43 PM
 */

namespace App\Repositories;


use App\Models\Order;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;

class TransactionRepository
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    private $queryParams = ['user_id', 'status', 'before', 'after', 'reference'];

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param array $transactionData
     * @return bool
     */
    public function saveTransaction(array $transactionData)
    {
        $transaction = new Transaction($transactionData);
        return $transaction->save();
    }

    /**
     * @param Order $order
     * @param array $orderData
     * @param array $transactionData
     * @return array
     * @throws \Exception
     */
    public function saveOrderTransaction(Order $order, array $orderData, array $transactionData)
    {
        try {
            \DB::beginTransaction();
            $this->orderRepository->updateOrder($order, $orderData);
            $this->saveTransaction($transactionData);
            \DB::commit();
            return ['status' => true];
        } catch (\Exception $exception) {
            \DB::rollBack();
            throw $exception;
        }

    }

    /**
     * @param $transactionRef
     * @return Order|null
     *
     * retrieves order with transaction reference
     */
    public function getOrder($transactionRef)
    {
        $order = Order::where('transaction_ref', '=', $transactionRef);
        if ($order) {
            return $order->first();
        }
        return null;
    }

    /**
     * @param $params
     * @return Collection
     */
    public function getTransactions($params)
    {
        $page = 1;
        $perPage = 10;
        if (empty($params)) {
            return Transaction::paginate($perPage, ['*'], 'page', $page);
        }
        if (isset($params['user'])) {
            $params['user_id'] = $params['user'];
            unset($params['user']);
        }
        if(isset($params['page'])){
            $page = $params['page'];
        }
        if(isset($params['per_page'])){
            $perPage = $params['per_page'];
        }
        $keys = array_keys($params);
        $paramKeys = collect($keys);
        $filtered = $paramKeys->filter(function ($key) {
            return in_array($key, $this->queryParams);
        });
        if($filtered->isEmpty()){
            return Transaction::paginate($perPage, ['*'], 'page', $page);
        }
        $first = $filtered->first();
        if($first == 'before'){
            $transactions = Transaction::whereDate('created_at', '<', $params[$first]);
        }else if($first == 'after'){
            $transactions = Transaction::whereDate('created_at', '>=', $params[$first]);
        }else{
            $transactions = Transaction::where($first, $params[$first]);
        }
        if (is_null($transactions)) {
            return collect();
        }
        $filtered->except([$first])
            ->each(function ($key) use ($transactions, $params) {
                if (is_null($transactions)) {
                    return;
                }
                if ($key == 'before') {
                    $transactions->whereDate('created_at', '<=', $params[$key]);
                } else if ($key == 'after') {
                    $transactions->whereDate('created_at', '>=', $params[$key]);
                } else {
                    $transactions->where($key, '=', $params[$key]);
                }
            });
        if(is_null($transactions)){
            return collect();
        }
        return $transactions->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * @return OrderRepository
     */
    public function getOrderRepository(): OrderRepository
    {
        return $this->orderRepository;
    }

    /**
     * @param OrderRepository $orderRepository
     * @return TransactionRepository
     */
    public function setOrderRepository(OrderRepository $orderRepository): TransactionRepository
    {
        $this->orderRepository = $orderRepository;
        return $this;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @param array $queryParams
     * @return TransactionRepository
     */
    public function setQueryParams(array $queryParams): TransactionRepository
    {
        $this->queryParams = $queryParams;
        return $this;
    }


}