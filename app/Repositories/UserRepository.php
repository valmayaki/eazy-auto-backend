<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 26/05/2019
 * Time: 11:17 PM
 */

namespace App\Repositories;


use App\User;

class UserRepository
{
    private $queryParams = ['email', 'name'];

    /**
     * @var UserDocumentRepository
     */
    protected $documentRepository;

    public function findByEmail($email){
        $user = User::where('email', '=', $email);
        if(is_null($user)){
            return null;
        }
        return $user->first();
    }

    public function find($id){
        return User::find($id);
    }

    public function search($params){
        $page = 1;
        $perPage = 10;
        if(isset($params['page'])){
            $page = $params['page'];
        }
        if(isset($params['per_page'])){
            $perPage = $params['per_page'];
        }
        $keys = array_keys($params);
        $allowedKeys = collect($keys)->filter(function($key) {
            return in_array($key, $this->queryParams);
        });
        if($allowedKeys->isEmpty()){
            return User::paginate($perPage, ['*'], 'page', $page);
        }
        $first = $allowedKeys->first();
        $operator = $first == 'name' ? 'LIKE' : '=';
        $users = User::where($first, $operator, $params['first']);
        $allowedKeys->except([$first])->each(function($key) use ($users, $params){
            if(is_null($users)){
                return;
            }
            $operator = $key == 'name' ? 'LIKE' : '=';
            $users->where($key, $operator, $params[$key]);
        });
        if(is_null($users)){
            return collect();
        }
        return $users->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function deleteUser(User $user){
        $user->orders()->delete();
        $user->vehicles()->delete();
        $user->vendors()->delete();
        $user->cards()->delete();
        $user->transactions()->delete();
        $documents = $user->documents();
        $documentRepository = $this->getDocumentRepository();
        $documents->each(function($document) use ($documentRepository){
            $documentRepository->removeDocument($document);
        });
        return $user->delete();
    }

    public function getDocumentRepository(){
        if(!$this->documentRepository){
            $this->documentRepository = resolve(UserDocumentRepository::class);
        }
        return $this->documentRepository;
    }
}