<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 16/06/2019
 * Time: 12:17 PM
 */

namespace App\Repositories;


use App\Models\Vendor;

class VendorRepository extends Repository
{
    /**
     * @var MetaDataRepository
     */
    private $metaDataRepository;

    public function __construct(MetaDataRepository $metaDataRepository)
    {
        $this->metaDataRepository = $metaDataRepository;
    }

    /**
     * @param array $attributes
     * @return Vendor
     */
    public function getVendorInstance(array $attributes = [])
    {
        return new Vendor($attributes);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getAllVendors(array $params)
    {
        $where = $this->buildParams($params);
        $perPage = isset($params['per_page']) ? $params['per_page'] : $this->perPage;
        $page = isset($params['page']) ? $params['page'] : null;
        $vendor = Vendor::where($where);
        if (isset($params['search'])) {
            $vendor = $this->getSearchParams($vendor, $params['search']);
        }
        return $vendor->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * @param array $vendorData
     * @return Vendor
     */
    public function createVendor(array $vendorData)
    {
        $type = array_key_exists('type', $vendorData) ? $vendorData['type'] : Vendor::CUSTOMER_VENDOR;
        $vendor = $this->getVendorInstance();
        $vendor->fill($vendorData);
        $success = $vendor->save();
        if($success == false){
            return null;
        }
        $default = array_key_exists('default', $vendorData) ? boolval($vendorData['default']) : false;
        if ($type == Vendor::ADMIN_VENDOR && $default) {
            // update default vendor in meta_data
            $this->metaDataRepository->updateMetaData(Vendor::DEFAULT_META_DATA_KEY, $vendor->id);
        }
        return $vendor;
    }

    /**
     * @param Vendor $vendor
     * @param array $vendorData
     * @return Vendor
     */
    public function updateVendor(Vendor $vendor, array $vendorData)
    {
        $type = $vendor->type;
        $default = array_key_exists('default', $vendorData) ? boolval($vendorData['default']) : false;
        $vendor->fill($vendorData);
        $success = $vendor->save();
        if($success == false){
            return null;
        }
        if ($type == Vendor::ADMIN_VENDOR) {
            $defaultVendor = $this->metaDataRepository->getMetaValue(Vendor::DEFAULT_META_DATA_KEY); // get default vendor from meta_data table
            if ($default) {
                // update default vendor on meta_data table
                $this->metaDataRepository->updateMetaData(Vendor::DEFAULT_META_DATA_KEY, $vendor->id);
            }
            if ($defaultVendor == $vendor->id) {
                if (array_key_exists('default', $vendorData) && !boolval($vendorData['default'])) {
                    // unset $vendor as default
                    $this->metaDataRepository->deleteMetaData(Vendor::DEFAULT_META_DATA_KEY);
                }
            }
        }
        if (array_key_exists('type', $vendorData)) {
            unset($vendorData['type']); // vendor type cannot be updated
        }

        return $vendor;
    }

    /**
     * @return Vendor|null
     */
    public function getDefaultVendor()
    {
        $defaultVendorId = $this->metaDataRepository->getMetaValue(Vendor::DEFAULT_META_DATA_KEY);
        if (is_null($defaultVendorId)) {
            return null;
        }
        return Vendor::find($defaultVendorId);
    }

    /**
     * @param Vendor $vendor
     * @return bool
     */
    public function isDefaultVendor(Vendor $vendor)
    {
        $default = $this->metaDataRepository->getMetaValue(Vendor::DEFAULT_META_DATA_KEY);
        return $vendor->id == $default;
    }

    /**
     * @param Vendor $vendor
     * @return bool|null
     * @throws \Exception
     */
    public function deleteVendor(Vendor $vendor)
    {
        $deleted = $vendor->delete();
        if ($deleted) {
            $this->metaDataRepository->deleteMetaData(Vendor::DEFAULT_META_DATA_KEY);
        }
        return $deleted;
    }

    /**
     * @return MetaDataRepository
     */
    public function getMetaDataRepository(): MetaDataRepository
    {
        return $this->metaDataRepository;
    }

    /**
     * @param MetaDataRepository $metaDataRepository
     * @return VendorRepository
     */
    public function setMetaDataRepository(MetaDataRepository $metaDataRepository): VendorRepository
    {
        $this->metaDataRepository = $metaDataRepository;
        return $this;
    }


    /**
     * @param $params
     * @return array
     */
    protected function buildParams($params)
    {
        $where = [];
        if(isset($params['before'])) {
            $where[] = ['created_at', '<=', $params['before']];
        }
        if(isset($params['after'])) {
            $where[] = ['created_at', '>=', $params['after']];
        }
        if(isset($params['user'])) {
            $where[] = ['added_by', '=', $params['user']];
        }
        return $where;
    }

    private function getSearchParams($model, $search)
    {
        $model->orWhere('role', 'like', "%$search%")
            ->orWhere('name', 'like', "%$search%")
            ->orWhere('phone_number', 'like', "%$search%")
            ->orWhere('email', 'like', "%$search%")
            ->orWhere('address_1', 'like', "%$search%")
            ->orWhere('address_2', 'like', "%$search%")
            ->orWhere('city', 'like', "%$search%");
        return $model;
    }
}