<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 23/05/2019
 * Time: 3:31 AM
 */

namespace App\Repositories;


use App\Models\OrderItem;

class OrderItemRepository
{

    public function getOrderItem($id){
        return OrderItem::find($id);
    }

    public function getOrderItems($orderId){
        return OrderItem::where('order_id', '=', $orderId)->get();
    }
}