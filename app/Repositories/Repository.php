<?php
/**
 * Created by PhpStorm.
 * User: Victor
 * Date: 15/06/2019
 * Time: 8:27 PM
 */

namespace App\Repositories;


class Repository
{
    protected $perPage = 10;

    protected function buildParams($params) {
        $where = [];
        if(isset($params['before'])) {
            $where[] = ['created_at', '<=', $params['before']];
        }
        if(isset($params['after'])) {
            $where[] = ['created_at', '>=', $params['after']];
        }
        if(isset($params['user'])) {
            $where[] = ['user_id', '=', $params['user']];
        }
        return $where;
    }
}