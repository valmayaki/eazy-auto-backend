#!/usr/bin/env bash

set +e

export M_USERNAME=homestead;
export M_PASSWORD=secret
export M_DATABASE=homestead
docker  run -it --name testdb1  \
    -e MYSQL_ROOT_PASSWORD=${M_PASSWORD} \
    -e MYSQL_DATABASE=${M_DATABASE} \
    -e MYSQL_USERNAME=${M_USERNAME} \
    -e MYSQL_PASSWORD=${M_PASSWORD} \
    -d mysql:5.7;

failcounter=0
timeout_in_sec=120
until docker exec testdb1 mysqladmin --user=root --password=${M_PASSWORD} --host "127.0.0.1" ping --silent &> /dev/null ; do
    let "failcounter += 1"
    echo "Waiting for database connection..."
     if [[ $failcounter -gt timeout_in_sec ]]; then
      echo "Timeout ($timeout_in_sec seconds) reached; failed to connect to database"
      exit 1
    fi
    sleep 2
done
docker run -it  --rm \
    --volume $PWD:/var/www/html \
    --workdir /var/www/html \
    --link testdb1 \
    -e DB_HOST=testdb1 \
    -e DB_DATABASE=${M_DATABASE} \
    -e DB_USERNAME=root \
    -e DB_PASSWORD=${M_PASSWORD} \
    app-php:1.0.3 sh -c "php artisan route:list && php artisan migrate && php ./vendor/bin/phpunit --testdox $@";

docker stop testdb1 && docker rm testdb1;