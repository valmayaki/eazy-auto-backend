FROM php:7.1-fpm

RUN apt-get update && apt-get install -y \
	libmcrypt-dev curl zip sqlite \
	git unzip wget\
    && docker-php-ext-install -j$(nproc) mbstring mcrypt tokenizer pdo pdo_mysql \
	&& apt-get -y autoremove \
	&& apt-get clean \
	&& rm -rf  /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN curl -sS https://getcomposer.org/installer | php \
	&& \
	mv composer.phar /usr/bin/composer
WORKDIR /var/www/html/
