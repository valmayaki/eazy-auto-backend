<?php
/**
 * Created by PhpStorm.
 * User: Valentine
 * Date: 19/12/2018
 * Time: 6:01 AM
 */

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;
    /**
     * POST /v1/register
     * Should create a new user and return a 200 with token
     *
     * @return void
     */
    public function testUsersCanRegister()
    {
        $this->withoutExceptionHandling();
        $user = [
            'first_name' => 'Max',
            'last_name' => 'Micheal',
            'email' => 'example@example.com',
            'phone_number' => '2347054346790',
            'password' => 'obonofelele',
            'gender' => 'male'
        ];
        $response = $this->registerUser($user);
        $response->assertOk();
        $response->assertJson([
            'error' => false,
        ]);
        $this->isAuthenticated('jwt');

    }
    /**
     * POST /v1/login
     * 
     * Should authenticate user and return a 200 with token
     *
     * @return void
     */
    public function testUsersCanLogin()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $response = $this->loginUser($user->email);
        $response->assertOk();
        $this->isAuthenticated('jwt');
    }

    /**
     * Post /v2/login
     * 
     * Should return a error with a status 401
     *
     * @return void
     */
    public function testGivesErrorForInvalidCredential()
    {
       $this->withoutExceptionHandling();
        $response = $this->json('POST', '/v1/login', [
            'email' => 'unregistered@email.com',
            'password' => 'secret'
        ]);
        $response->assertStatus(401);
    }
    
    /**
     * POST /v1/logout
     * 
     * Should return a 200 response
     *
     * @return void
     */
    public function testUsersCanLogout()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['activated'=>true]);
        $response = $this->loginUser($user->email);
        $token = $response->json('token');
        $response =
            $this->withHeader('Authorization', sprintf('Bearer %s', $token))
            ->json('POST','/v1/logout');
        $response->assertOk();
    }

    /**
     * Registers a User
     *
     * @param $user
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function registerUser($user)
    {
        $response = $this->json('POST', '/v1/register', $user);
        return $response;
    }

    /**
     * @param $email
     * @param string $password
     * @return \Illuminate\Foundation\Testing\TestResponse
     */
    protected function loginUser($email, $password = 'secret')
    {

        $response = $this->json('POST', '/v1/login', [
            'email' => $email,
            'password' => $password
        ]);
        return $response;
    }

}