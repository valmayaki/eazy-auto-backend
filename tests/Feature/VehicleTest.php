<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Models\Vehicle;

class VehicleEndpointTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUsersCanAddAVehicleInformation()
    {
        // $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $response = $this
                        ->actingAsViaApi($user)
                        ->json(
                            'POST',
                            '/v1/vehicles',
                            [ 'plate_number'=> '1234']
                        );
        $response
                ->assertStatus(201)
                ->assertJson([
                    'plate_number' => 1234,
                ]);
    }

    public function testUsersCanGetAllTheirSavedVehicles()
    {
        $vehicle = factory(Vehicle::class)->create();
        $response = $this
                        ->actingAsViaApi($vehicle->owner)
                        ->json('GET', '/v1/vehicles');
        $response
                ->assertOk()
                ->assertJsonCount(1, 'data');
        
    }

    public function testUsersCanGetInformationOfOneOfTheirVehicle()
    {
        $vehicle = factory(Vehicle::class)->create();
        $response = $this
                        ->actingAsViaApi($vehicle->owner)
                        ->json('GET', sprintf('/v1/vehicles/%s', $vehicle->id));
        $response
                ->assertOk()
                ->assertJson([
                    'plate_number' => $vehicle->plate_number,
                    'id' => $vehicle->id
                ]);
        
    }
    public function testUsersCanUpdateInformationOfOneOfTheirVehicle()
    {
        $plateNumber = "123456";
        $vehicle = factory(Vehicle::class)->create();
        $response = $this
                        ->actingAsViaApi($vehicle->owner)
                        ->json(
                            'PUT',
                            sprintf('/v1/vehicles/%s',$vehicle->id),
                            ['plate_number' => $plateNumber]
                        );
        $response
                ->assertOk()
                ->assertJsonMissing(['plate_number' => $vehicle->plate_number])
                ->assertJson([
                    'plate_number' => $plateNumber,
                    'id' => $vehicle->id
                ]);
        
    }
    public function testUsersCanDeleteTheirVehicleInformation()
    {
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
          );
    }

    public function testAnotherUserCanNotUpdateAUserVehicleWithoutAPriviledgedAccess()
    {
        $this->markTestIncomplete(
            'This test has not been implemented yet.'
          );
    }
}
