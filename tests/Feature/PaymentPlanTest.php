<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\PaymentPlan;
use App\User;

class PaymentPlanTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUsersCanGetAvailablePaymentPlans()
    {
        $user = factory(User::class)->create();
        $paymentPlan = \factory(PaymentPlan::class)->create();
        $response = $this
                        ->actingAsViaApi($user)
                        ->json('GET', '/v1/payment-plans');
        $response
                ->assertOk()
                ->assertJsonCount(1);
    }
    public function testUsersWithPriviledgeCanAddPaymentPlans()
    {
        $user = factory(User::class)->create();
        $plan = [
            "name"=> "1 month loan",
            "months"=> 1,
            "interest"=> 5,
            "interest_type"=> "interest on remaining"
        ];
        $response = $this
                        ->actingAsViaApi($user)
                        ->json(
                            'POST',
                            '/v1/payment-plans',
                           $plan
                        );
        $response
                ->assertStatus(201)
                ->assertJson([
                    'name' => $plan['name'],
                    'months' => $plan['months'],
                    "interest"=> $plan['interest'],
                ]);
        $this->markTestIncomplete(
            'This test need to check a user priviledge has payment plan create access.'
        );
    }
}
