<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUsersCanViewThierBio()
    {
        $user = \factory(User::class)->create();
        $response = $this->actingAsViaApi($user)
                        ->json('GET', '/v1/user');
        $response->assertOk()
                ->assertJson([
                    'name' => $user->name,
                    'email'=> $user->email,
                    'address_1' => $user->address_1,
                    'address_2' => $user->address_2,
                    'city' => $user->city,
                    'state' => $user->state,
                    'country' => $user->country,
                    'gender' => $user->gender,
                    'phone_number' => $user->phone_number,
                ]);
    }
}
