<?php

namespace Tests\Feature;

use App\Events\OrderCreated;
use App\Repositories\OrderRepository;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Models\Vendor;
use App\Models\Vehicle;
use App\Models\ServiceType;

class OrderTest extends TestCase
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;
    public function setUp()
    {
        parent::setUp();
        $this->orderRepository = \Mockery::mock(OrderRepository::class);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCanCreateOrderWithValidParameters()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create();
        $order = [
            'payment_method' => 'loan',
            'order_items' => [
                [
                    'vendor_id' => $this->faker->randomDigit,
                    'vehicle_id' => $this->faker->randomDigit,
                    'service_type' => $this->faker->randomDigit,
                    'amount' => 1000
                ]
            ]
        ];
        \Event::fake([OrderCreated::class]);
        $validator = \Mockery::mock('stdClass');
        \Validator::swap($validator);
        $validator->shouldReceive('make')->once()->andReturn($validator);
        $validator->shouldReceive('fails')->once()->andReturn(false);
        $this->orderRepository->shouldReceive('createOrder')->andReturn($order);
        $response = $this->actingAsViaApi($user)->json('POST', '/v1/orders', $order);
        \Event::assertDispatched(OrderCreated::class);
        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testUnableToCreateOrderWithInvalidParameters(){
        $user = factory(User::class)->create();
        $order = [
            'payment_method' => 'loan',
            'order_items' => [
                [
                    'vendor_id' => -1,
                    'vehicle_id' => -1,
                    'service_type' => -1,
                    'amount' => 1000
                ]
            ]
        ];
        \Event::fake([OrderCreated::class]);
        $response = $this->actingAsViaApi($user)->json('POST', '/v1/orders', $order);
        \Event::assertNotDispatched(OrderCreated::class);
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

}
