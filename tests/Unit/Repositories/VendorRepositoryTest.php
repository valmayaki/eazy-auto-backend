<?php
/**
 * Created by Victor.
 * User: Victor
 * Date: 24/08/2019
 * Time: 11:22 AM
 */

namespace Tests\Unit\Repositories;


use App\Models\Vendor;
use App\Repositories\MetaDataRepository;
use App\Repositories\VendorRepository;
use App\User;
use Tests\TestCase;

class VendorRepositoryTest extends TestCase
{

    private $table = 'vendors';
    /**
     * @var VendorRepository
     */
    private $vendorRepository;


    private $metaDataRepository;

    public function setUp()
    {
        parent::setUp();
        factory(User::class, 1)->create();
        $this->metaDataRepository = \Mockery::mock(MetaDataRepository::class);
        $this->vendorRepository = new VendorRepository($this->metaDataRepository);
    }

    public function testCreateVendorForCustomer(){
        $data = $this->getVendorData();
        $this->metaDataRepository->shouldNotReceive('updateMetaData');
        $vendor = $this->vendorRepository->createVendor($data);
        $this->assertDatabaseHas($this->table, $data);
        $this->assertNotNull($vendor);
    }

    public function testCreateVendorForAdminWithDefault(){
        $data = $this->getVendorData(Vendor::ADMIN_VENDOR);
        $data['default'] = true;
        $this->metaDataRepository->shouldReceive('updateMetaData');
        $vendor = $this->vendorRepository->createVendor($data);
        $this->assertNotNull($vendor);
    }

    public function testUpdateVendorForCustomer(){
        $data = $this->getVendorData();
        $oldName = $data['name'];
        $vendor = factory(Vendor::class)->create($data);
        $this->assertEquals($data['name'], $vendor->name);
        $data['name'] = $this->faker->name;
        $data['phone_number'] = $this->faker->phoneNumber;
        $this->metaDataRepository->shouldNotReceive('getMetaData');
        $result = $this->vendorRepository->updateVendor($vendor, $data);
        $this->assertEquals($result->name, $data['name'], 'name property of vendor not updated for customer');
        $this->assertNotEquals($oldName, $vendor->name, 'name property not updated');
    }

    public function testUpdateVendorForAdmin(){
        $data = $this->getVendorData(Vendor::ADMIN_VENDOR);
        $oldName = $data['name'];
        $vendor = factory(Vendor::class)->create($data);
        $this->assertEquals($data['name'], $vendor->name);
        $data['name'] = $this->faker->name;
        $data['phone_number'] = $this->faker->phoneNumber;
        $this->metaDataRepository->shouldReceive('getMetaValue')->once()->andReturn(null);
        $this->metaDataRepository->shouldNotReceive('updateMetaData');
        $result = $this->vendorRepository->updateVendor($vendor, $data);
        $this->assertEquals($result->name, $data['name'], 'name property of vendor not updated for customer');
        $this->assertNotEquals($oldName, $vendor->name, 'name property not updated');
        $data['default'] = true;
        $this->metaDataRepository->shouldReceive('getMetaValue')->once()->andReturn($vendor->id);
        $this->metaDataRepository->shouldReceive('updateMetaData')->withArgs([Vendor::DEFAULT_META_DATA_KEY, $vendor->id]);
        $result = $this->vendorRepository->updateVendor($vendor, $data);
        $this->assertEquals($result->name, $data['name'], 'name property of vendor not updated for customer');
        $this->assertNotEquals($oldName, $vendor->name, 'name property not updated');
    }

    private function getVendorData($type = Vendor::CUSTOMER_VENDOR){
        return  [
            'name' => $this->faker->name,
            'role' => $this->faker->name,
            'phone_number' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'address_1' => $this->faker->address,
            'address_2' => '',
            'city' => $this->faker->city,
            'state' => $this->faker->city,
            'country' => 'Nigeria',
            'bank' => $this->faker->randomElement(['GTB', 'Zenith', 'First bank', 'access']),
            'bank_account_no' => $this->faker->bankAccountNumber,
            'type' => $type,
            'added_by' => User::all()->random()->id,
        ];
    }
}