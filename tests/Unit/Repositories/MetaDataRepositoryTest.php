<?php
namespace Tests\Unit\Repositories;
use App\Models\Metadata;
use App\Repositories\MetaDataRepository;
use Tests\TestCase;

/**
 * Created by Victor.
 * User: Victor
 * Date: 24/08/2019
 * Time: 10:19 AM
 */
class MetaDataRepositoryTest extends TestCase
{
    /**
     * @var MetaDataRepository
     */
    private $metaDataRepository;
    private $tableName = 'meta_data';

    public function setUp()
    {
        parent::setUp();
        $this->metaDataRepository = new MetaDataRepository();
    }

    public function testUpdateMetaDataWhenMetaKeyDoesNotExist(){
        $meta_key = $this->faker->text(20);
        $meta_value = $this->faker->text(30);
        $this->metaDataRepository->updateMetaData($meta_key, $meta_value);
        $this->assertDatabaseHas($this->tableName, ['meta_key' => $meta_key]);
    }

    public function testUpdateMetaDataWhenMetaKeyExists(){
        $meta_key = $this->faker->text(20);
        $meta_value = $this->faker->text(30);
        $this->metaDataRepository->updateMetaData($meta_key, $meta_value);
        $meta_value = 'updated_meta_value';
        $this->metaDataRepository->updateMetaData($meta_key, $meta_value);
        $this->assertDatabaseHas($this->tableName, ['meta_key' => $meta_key]);
        $metadataCol = Metadata::where('meta_key', $meta_key);
        $this->assertNotNull($metadataCol);
        $this->assertNotEmpty($metadataCol, 'Empty result returned for present meta_key');
        $this->assertEquals(1, count($metadataCol), 'More than one meta_key exist on MetaData table');
        $metadata = $metadataCol->first();
        $this->assertNotNull($metadata);
        $metaVal = $metadata->meta_value;
        $this->assertEquals($metaVal, $meta_value, 'MetaValue not updated if meta_key already exist on the meta_data table');
    }

    public function testGetMetaValueWhenMetaKeyDoesNotExist(){
        $metaKey = 'invalid_meta_key';
        $metaValue = $this->metaDataRepository->getMetaValue($metaKey);
        $this->assertNull($metaValue);
    }

    public function testGetMetaValueWhenMetaKeyExist(){
        $metaKey = $this->faker->text(20);
        $metaValue = $this->faker->text(30);
        factory(Metadata::class)->create([
            'meta_key' => $metaKey,
            'meta_value' => $metaValue
        ]);
        $this->assertDatabaseHas($this->tableName, ['meta_key' => $metaKey]);
        $newMetaValue = $this->metaDataRepository->getMetaValue($metaKey);
        $this->assertNotNull($newMetaValue);
        $this->assertEquals($metaValue, $newMetaValue, 'meta_value inserted not the same as meta_value retrieved');
    }

    public function testGetMetaValueWhenValueIsArray(){
        $metaKey = $this->faker->text(20);
        $metaValArr = ['first' => 'a', 'second' => 'b'];
        $metaValue = json_encode($metaValArr);
        factory(Metadata::class)->create([
            'meta_key' => $metaKey,
            'meta_value' => $metaValue
        ]);
        $this->assertDatabaseHas($this->tableName, ['meta_key' => $metaKey]);
        $newMetaValue = $this->metaDataRepository->getMetaValue($metaKey);
        $this->assertNotNull($newMetaValue);
        $this->assertSame($metaValArr, $newMetaValue, 'returned meta value not the same as input meta_value');
    }

    public function testDeleteMetaKey(){
        $metaKey = $this->faker->text(20);
        $metaValue = $this->faker->text(20);
        factory(Metadata::class)->create([
            'meta_key' => $metaKey,
            'meta_value' => $metaValue
        ]);
        $result = $this->metaDataRepository->deleteMetaData($metaKey);
        $this->assertDatabaseMissing($this->tableName, ['meta_key' => $metaKey]);
        $this->assertTrue($result, 'meta_key not deleted');
    }
}