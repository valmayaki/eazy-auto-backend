<?php

Route::prefix('v1')->group(function () {
    Route::post('/login', "Api\JWTAuthController@login");
    Route::post('/register', "Api\JWTAuthController@register");
    Route::post('/logout', 'Api\JWTAuthController@logout');
    Route::get('/resend-activation', ['use' => 'Api\JWTAuthController@resendActivation'])->name('auth.resend_activation');

    Route::middleware('jwt.auth')->group(function () {
        Route::namespace('Api')->group(function(){
            //---------------------------------------------- USERS --------------------------------------------------
            Route::prefix('users')->group(function () {
                Route::get('/', 'UserController@index')->name('users');
                Route::get('/{user}', 'UserController@show')->name('user.show');
                Route::put('/{user}', 'UserController@update')->name('user.update');
                Route::delete('/{user}', 'UserController@destroy')->name('user.delete');
            });

            //---------------------------------------------- VEHICLES --------------------------------------------------
            Route::prefix('vehicles')->group(function () {
                Route::get('/', 'VehicleController@index')->name('vehicles');
                Route::post('/', 'VehicleController@store')->name('vehicles.store');
                Route::get('/{vehicle}', 'VehicleController@show')->name('vehicle.show');
                Route::put('/{vehicle}', 'VehicleController@update')->name('vehicle.update');
                Route::delete('/{vehicle}', 'VehicleController@destroy')->name('vehicle.delete');
            });

            //---------------------------------------------- VENDORS --------------------------------------------------
            Route::prefix('vendors')->group(function () {
                Route::get('/', 'VendorController@index')->name('vendors');
                Route::post('/', 'VendorController@store')->name('vendor.store');
                Route::get('/default', 'VendorController@getDefaultVendor')->name('vendor.default');
                Route::get('/{vendor}', 'VendorController@show')->where('vendor', '[0-9]+')->name('vendor.show');
                Route::match(['post', 'put'], '/{vendor}', 'VendorController@update')->name('vendor.update');
                Route::delete('/{vendor}', 'VendorController@destroy')->name('vendor.delete');
            });

            //---------------------------------------------- PAYMENT PLANS -------------------------------------------------
            Route::prefix('payment-plans')->group(function () {
                Route::get('/', 'PaymentPlanController@index');
                Route::get('/{paymentPlan}', 'PaymentPlanController@show')->name('payment-plan.show');
                Route::post('/', 'PaymentPlanController@store')->name('payment-plan.store');
                Route::put('/{paymentPlan}', 'PaymentPlanController@update')->name('payment-plan.update');
                Route::delete('/{paymentPlan}', 'PaymentPlanController@destroy')->name('payment-plan.destroy');
            });

            //---------------------------------------------- SERVICE TYPES -------------------------------------------------

            Route::prefix('service-types')->group(function () {
                Route::get('/', 'ServiceTypeController@index')->name('service-type');
                Route::get('/{serviceType}', 'ServiceTypeController@show')->name('service-type.show');
                Route::post('/', 'ServiceTypeController@store')->name('service-type.store');
                Route::put('/{serviceType}', 'ServiceTypeController@update')->name('service-type.update');
                Route::delete('/{serviceType}', 'ServiceTypeController@destroy')->name('service-type.delete');
            });

            //---------------------------------------------- ORDERS --------------------------------------------------
            Route::prefix('orders')->group(function(){
                Route::get('/', 'OrderController@index')->name('order');
                Route::put('/{order}', 'OrderController@update')->name('order.update');
                Route::get('/{order}', 'OrderController@show')->name('order.show');
                Route::delete('/{order}', 'OrderController@destroy')->name('order.delete');
                Route::post('/', 'OrderController@create')->name('order.store');
            });

            //---------------------------------------------- USER CARDS --------------------------------------------------
            Route::prefix('card')->group(function(){
                Route::get('/', 'UserCardController@index')->name('user-card');
                Route::get('/{userCard}', 'UserCardController@show')->name('user-card.show');
                Route::post('/', 'UserCardController@create')->name('user-card.store');
                Route::delete('/{userCard}', 'UserCardController@destroy')->name('user-card.delete');
                Route::post('/default/{userCard}', 'UserCardController@setCardDefault')->name('user-card.set-default');
            });

            //---------------------------------------------- VERIFICATION --------------------------------------------------

            Route::get('/verify/bvn/{bvn}', 'Accounts\VerificationController@resolveBVN')->name('verify.bvn');
            Route::get('/verify/payment/{reference}', 'Accounts\VerificationController@verifyPayment')->name('verify.payment');

            //---------------------------------------------- TRANSACTION --------------------------------------------------

            Route::prefix('transactions')->group(function(){
                Route::get('/', 'TransactionController@index')->name('transaction');
            });

            //---------------------------------------------- USER DOCUMENTS --------------------------------------------------
            Route::prefix('documents')->group(function(){
                Route::get('/', 'UserDocumentController@index')->name('user-document');
                Route::get('/{userDocument}', 'UserDocumentController@show')->name('user-document.delete');
                Route::post('/', 'UserDocumentController@create')->name('user-document.store');
                Route::delete('/{userDocument}', 'UserDocumentController@destroy')->name('user-document.delete');
            });
        });
        

    });

});