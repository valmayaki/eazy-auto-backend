<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

require __DIR__.'/api-versions/v1.php';
Route::prefix('/auth')->group(function(){
    Route::post('/request-password-reset', ['uses'=> 'Api\JWTAuthController@sendResetLinkEmail']);
    Route::post('/password-reset', ['uses'=> 'Api\JWTAuthController@reset']);
});
Route::get('user/activation/{token}', 'Api\ActivationController@activateUser')->name('user.activate');