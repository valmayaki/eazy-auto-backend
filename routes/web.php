<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('mailable', function () {
    $order = App\Models\Order::find(1);
    return new App\Mail\OrderCreatedInvoice($order);
});

Route::get('processinvoice/{encoded}', 'InvoiceController')->name('process_invoice');
//Auth::routes();
//Route::get('/transaction', 'Api\Accounts\VerificationController@handleTransaction');

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('login/{provider}', 'SocialLoginController@redirect');
Route::get('redirect/{provider}/callback', 'SocialLoginController@handleCallback');
